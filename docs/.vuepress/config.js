// .vuepress/config.js
module.exports = {
    title: 'vue-custom-pages开发文档',
    description: 'Just playing around',
    themeConfig: {
      nav: [
        { text: 'Home', link: '/' },
        { text: 'Guide', link: '/guide/' },
        { text: 'GIT', link: 'https://gitee.com/it__distant_branch/vue-custom-pages' },
      ],
      sidebar: [
        '/',
        '/page-a',
        ['/page-b', 'Explicit link text']
      ],
      sidebarDepth: 2
      
    }
  }