# vue-custom-pages

> 基于vue2.0实现的自定义分页

> 实现效果如下：
    ![](https://images.gitee.com/uploads/images/2020/0807/153635_ce2a1c31_1652493.png "QQ图片20200807153234.png")

> 支持功能

    - [x] 自定义分页条数
    - [x] 跳转某一页


> 联系方式 

    - 微信:18888320271

#### 快速上手

    1. npm安装  `npm i --save vue-custom-pages`
    2. main.js中引入 ` 
                import customPages from 'vue-custom-pages';
                Vue.use(customPages)
                 `
    3. 使用
      `
      <template>
          <div id="app">
            <vue-custom-pages :total="1000" active-color="red" page-size="20" show-count v-model="page" @change="search"          show-elevator :size='[10,20,30,40]'   ></vue-custom-pages>
      </div>
    </template>
    
    <script>
    export default {
      name: 'app',
      methods:{
        search(page,pageSzie){
            console.log("当前页：",page)
            console.log("当前页大小：",pageSzie)
        }
      },
      data () {
        return {
          page:1
          
        }
      }
    }
    </script>
    `
#### 参数说明
| 参数            | 说明           | 类型      | 默认值     | 可选值  |
|---------------|--------------|---------|---------|------|
| total         | 总条数          | Number  | String  | 0    |
| page-size     | 页大小          | Number  | String  | 10   |
| show-elevator | 是否显示快速跳转到某一页 | Boolean | false   | true |
| v-model       | 当前页          | Number  | String  | 1    |
| size          | 可以切换每页显示的数量  | Array   | []      |      |
| show-count    | 是否显示总页数      | Boolean | false   | true |
| active-color  | 选中页背景色       | String  | #2979ff |      |
| next-name     | 下一页按钮名称      | String  | >       |      |
| pre-name      | 上一页按钮名称      | String  | <       |      |
| font-size      | 字体大小      | String  | 12px       |      |
| show-current-page      | 是否只展示当前页码 (1.1.2版本新增+)      | Boolean  | false       |      |
