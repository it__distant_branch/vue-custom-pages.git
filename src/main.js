import Vue from 'vue'
import App from './App.vue'
import vueCustomPages from './lib/index';
Vue.use(vueCustomPages)
new Vue({
  el: '#app',
  render: h => h(App)
})
